package com.pixeltk.yourplayer.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pixeltk.yourplayer.R;

public class Registration extends AppCompatActivity {

    EditText validation;
    Button send;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        validation = (EditText) findViewById(R.id.validation_et);
        text = (TextView) findViewById(R.id.phone_num);
        send = (Button) findViewById(R.id.validation_bt);
    }
}
