package com.pixeltk.yourplayer.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pixeltk.yourplayer.Adapters.TrackAdapter;
import com.pixeltk.yourplayer.Api.Api;
import com.pixeltk.yourplayer.Api.getHttpGet;
import com.pixeltk.yourplayer.Getters.Track;
import com.pixeltk.yourplayer.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class Favorites extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    ListView listView;

    Api api = new Api();
    private ArrayList<Track> steks;
    private ArrayList<Track> showingStudios = new ArrayList<>();
    private TrackAdapter trackAdapter;
    getHttpGet request = new getHttpGet();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        listView = (ListView) findViewById(R.id.favorites_lv);

        trackAdapter = new TrackAdapter(this, showingStudios);
        listView.setAdapter(trackAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        try {
            request();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void request() throws JSONException {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        SharedPreferences prfs = getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
        String user_id = prfs.getString("id", "");
        JSONArray data = new JSONArray(request.getHttpGet(api.completeUrl("favorite?salt=smdf9045jdsg54gjs54os59gjsog5453&user_id=" + user_id)));

        Gson gson = new Gson();
        steks = gson.fromJson(data.toString(), new TypeToken<List<Track>>() {
        }.getType());

        if (steks != null) {

            for (int i = 0; i < steks.size(); i++) {
                Track stek = steks.get(i);
                showingStudios.add(stek);
            }
            trackAdapter.notifyDataSetChanged();
        }

    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.main) {
            startActivity(new Intent(Favorites.this, MainActivity.class));
        } else if (id == R.id.liked) {

            startActivity(new Intent(Favorites.this, Favorites.class));

        } else if (id == R.id.radio) {
            startActivity(new Intent(Favorites.this, Radio.class));

        } else if (id == R.id.offline_player) {

            startActivity(new Intent(Favorites.this, Player.class));

        } else if (id == R.id.info) {
            startActivity(new Intent(Favorites.this, Info.class));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
