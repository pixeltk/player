package com.pixeltk.yourplayer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.pixeltk.yourplayer.R;
import com.pixeltk.yourplayer.equalizer.AudioFxActivity;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

public class Player extends AppCompatActivity {
    private Toolbar toolbar;
    ImageView play, left, right;
    RoundedImageView album;
    ImageButton shuffle, repeat, save, favorite;
    DiscreteSeekBar seekBar, volume;
    TextView songName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        play = (ImageView) findViewById(R.id.play_pause);
        left = (ImageView) findViewById(R.id.imageView2);
        right = (ImageView) findViewById(R.id.imageView3);
        album = (RoundedImageView) findViewById(R.id.roundedImageView);
        seekBar = (DiscreteSeekBar) findViewById(R.id.songProgressBar);
        volume = (DiscreteSeekBar) findViewById(R.id.volumeProgressBar);
        shuffle = (ImageButton) findViewById(R.id.btnShuffle);
        repeat = (ImageButton) findViewById(R.id.btnRepeat);
        save = (ImageButton) findViewById(R.id.btnDownload);
        favorite = (ImageButton) findViewById(R.id.btnfavorite);
        songName = (TextView) findViewById(R.id.textView2);

        toolbar = (Toolbar) findViewById(R.id.toolbar_player);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // получим идентификатор выбранного пункта меню
        int id = item.getItemId();
        // Операции для выбранного пункта меню
        switch (id) {
            case R.id.equalizer:
                startActivity(new Intent(Player.this, AudioFxActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
