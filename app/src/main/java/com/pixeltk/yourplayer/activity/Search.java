package com.pixeltk.yourplayer.activity;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pixeltk.yourplayer.Adapters.LanguageAdapter;
import com.pixeltk.yourplayer.Api.Api;
import com.pixeltk.yourplayer.Api.getHttpGet;
import com.pixeltk.yourplayer.Getters.Stek;
import com.pixeltk.yourplayer.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Search extends AppCompatActivity {

    ListView listView;
    EditText search;


    Api api = new Api();
    private ArrayList<Stek> steks;
    private ArrayList<Stek> showingStudios = new ArrayList<>();
    private LanguageAdapter languageAdapter;
    getHttpGet request = new getHttpGet();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


        listView = (ListView) findViewById(R.id.search_lv);
        search = (EditText) findViewById(R.id.editText_search);

        languageAdapter = new LanguageAdapter(this, showingStudios);
        listView.setAdapter(languageAdapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = search.getText().toString().toLowerCase();

                if (steks != null) {
                    steks.clear();
                    showingStudios.clear();
                    Log.d("myLogs", "ste " + steks.size());
                }
                if (text == null) {
                    if (Locale.getDefault().getDisplayLanguage().equals("oʻzbekcha")) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Bu sohada to'ldiring",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Пожалуйста заполните поле",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }

                } else {
                    try {
                        Log.d("myLogs", "text " + text);
                        request(text);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void request(String search) throws JSONException {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        JSONArray data = new JSONArray(request.getHttpGet(api.completeUrl("search?salt=smdf9045jdsg54gjs54os59gjsog5453&search=" + search)));

        Gson gson = new Gson();
        steks = gson.fromJson(data.toString(), new TypeToken<List<Stek>>() {
        }.getType());


        if (steks != null) {

            for (int i = 0; i < steks.size(); i++) {
                Stek stek = steks.get(i);
                showingStudios.add(stek);
            }
            languageAdapter.notifyDataSetChanged();

        }


    }
}
