package com.pixeltk.yourplayer.Getters;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user5 on 06.10.16.
 */

public class Stek {


    @SerializedName("id")
    String id;
    @SerializedName("name_rus")
    String name_rus;
    @SerializedName("name_uzbek")
    String name_uzbek;
    @SerializedName("icon")
    String icon;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName_rus() {
        return name_rus;
    }

    public void setName_rus(String name_rus) {
        this.name_rus = name_rus;
    }

    public String getName_uzbek() {
        return name_uzbek;
    }

    public void setName_uzbek(String name_uzbek) {
        this.name_uzbek = name_uzbek;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}
