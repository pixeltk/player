package com.pixeltk.yourplayer.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixeltk.yourplayer.Getters.Track;
import com.pixeltk.yourplayer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by o_cure on 1/23/16.
 */
public class TrackAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Track> tracks;
    private LayoutInflater layoutInflater;

    public TrackAdapter(Context context, ArrayList<Track> tracks) {
        this.context = context;
        this.tracks = tracks;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return tracks.size();
    }

    @Override
    public Object getItem(int position) {
        return tracks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_last, null);
        }
        Track tracks = (Track) getItem(position);


        ImageView imageView = (ImageView) view.findViewById(R.id.imageID);
        Picasso
                .with(context)
                .load(tracks.getIcon())
                .fit()
                .placeholder(R.drawable.ic_play)
                .into(imageView);

        TextView language = (TextView) view.findViewById(R.id.name_of_song);
        //вставить выбор языка
        if (Locale.getDefault().getDisplayLanguage().equals("oʻzbekcha")) {
            language.setText(tracks.getName_uzbek());
        } else {
            language.setText(tracks.getName_rus());
        }
        ImageButton like = (ImageButton) view.findViewById(R.id.like);


        return view;
    }
}
