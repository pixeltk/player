package com.pixeltk.yourplayer.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixeltk.yourplayer.Getters.Stek;
import com.pixeltk.yourplayer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by o_cure on 1/23/16.
 */
public class LanguageAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Stek> steks;
    private LayoutInflater layoutInflater;

    public LanguageAdapter(Context context, ArrayList<Stek> steks) {
        this.context = context;
        this.steks = steks;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return steks.size();
    }

    @Override
    public Object getItem(int position) {
        return steks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_ganres_language, null);
        }
        Stek steks = (Stek) getItem(position);

        Log.d("myLogs", "image " + steks.getIcon());

        ImageView imageView = (ImageView) view.findViewById(R.id.genre_language_image);
        Picasso
                .with(context)
                .load(steks.getIcon())
                .fit()
                .placeholder(R.drawable.icn)
                .into(imageView);

        TextView language = (TextView) view.findViewById(R.id.name_of_language);
        //вставить выбор языка
        if (Locale.getDefault().getDisplayLanguage().equals("oʻzbekcha")) {
            language.setText(steks.getName_uzbek());
        } else {
            language.setText(steks.getName_rus());
        }


        return view;
    }
}
