package com.pixeltk.yourplayer.fragment;


import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pixeltk.yourplayer.Adapters.TrackAdapter;
import com.pixeltk.yourplayer.Api.Api;
import com.pixeltk.yourplayer.Api.getHttpGet;
import com.pixeltk.yourplayer.Getters.Track;
import com.pixeltk.yourplayer.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Popular extends Fragment {

    ListView listView;

    Api api = new Api();
    private ArrayList<Track> tracks;
    private ArrayList<Track> showingStudios = new ArrayList<>();
    private TrackAdapter trackAdapter;
    getHttpGet request = new getHttpGet();
    public Popular() {
        // Required empty public constructor
    }

    public static Popular newInstance() {
        Popular fragment = new Popular();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_popular, container, false);

        listView = (ListView) view.findViewById(R.id.popular_lv);


        trackAdapter = new TrackAdapter(getContext(), showingStudios);
        listView.setAdapter(trackAdapter);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        try {
            JSONArray data = new JSONArray(request.getHttpGet(api.completeUrl("popular?salt=smdf9045jdsg54gjs54os59gjsog5453")));
            Log.d("myLogs", "data " + data);
            Gson gson = new Gson();
            tracks = gson.fromJson(data.toString(), new TypeToken<List<Track>>() {
            }.getType());

            showingStudios.clear();
            if (tracks != null) {

                for (int i = 0; i < tracks.size(); i++) {

                    Track track_url = tracks.get(i);
                    showingStudios.add(track_url);
                }
                trackAdapter.notifyDataSetChanged();


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {

                listView.setVisibility(View.GONE);
                /*
                тут интенет
                 */
            }
        });


        return view;
    }


}


