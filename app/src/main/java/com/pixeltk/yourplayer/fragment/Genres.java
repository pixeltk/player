package com.pixeltk.yourplayer.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pixeltk.yourplayer.Adapters.LanguageAdapter;
import com.pixeltk.yourplayer.Api.Api;
import com.pixeltk.yourplayer.Api.getHttpGet;
import com.pixeltk.yourplayer.Getters.Stek;
import com.pixeltk.yourplayer.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Genres extends Fragment {
    ListView listView;

    Api api = new Api();
    private ArrayList<Stek> steks;
    private ArrayList<Stek> showingStudios = new ArrayList<>();
    private LanguageAdapter languageAdapter;
    getHttpGet request = new getHttpGet();
    public Genres() {
        // Required empty public constructor
    }

    public static Genres newInstance() {
        Genres fragment = new Genres();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_genres2, container, false);

        listView = (ListView) view.findViewById(R.id.genres_lv);

        languageAdapter = new LanguageAdapter(getContext(), showingStudios);
        listView.setAdapter(languageAdapter);

        languageAdapter = new LanguageAdapter(getContext(), showingStudios);
        listView.setAdapter(languageAdapter);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        try {
            JSONArray data = new JSONArray(request.getHttpGet(api.completeUrl("genres?salt=smdf9045jdsg54gjs54os59gjsog5453")));

            Gson gson = new Gson();
            steks = gson.fromJson(data.toString(), new TypeToken<List<Stek>>() {
            }.getType());
            final String id_country[] = new String[steks.size() + 1];

            showingStudios.clear();

            if (steks != null) {

                for (int i = 0; i < steks.size(); i++) {
                    Stek stek = steks.get(i);
                    showingStudios.add(stek);
                    id_country[i] = String.valueOf(stek.getId());
                }
                languageAdapter.notifyDataSetChanged();


            }

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                        long id) {

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    GenresSongs firstFragment = GenresSongs.newInstance();

                    ft.replace(R.id.genres_fl, firstFragment, GenresSongs.class.getSimpleName());

                    ft.commit();
                    listView.setVisibility(View.GONE);

                    SharedPreferences preferences = getActivity().getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_WORLD_WRITEABLE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("id_genres", id_country[position]);
                    editor.apply();


                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }
}
