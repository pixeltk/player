package com.pixeltk.yourplayer.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pixeltk.yourplayer.Adapters.TrackAdapter;
import com.pixeltk.yourplayer.Api.Api;
import com.pixeltk.yourplayer.Api.getHttpGet;
import com.pixeltk.yourplayer.Getters.Track;
import com.pixeltk.yourplayer.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistsSongs extends Fragment {
    ListView listView;

    Api api = new Api();
    private ArrayList<Track> tracks;
    private ArrayList<Track> showingStudios = new ArrayList<>();
    private TrackAdapter trackAdapter;
    getHttpGet request = new getHttpGet();
    public ArtistsSongs() {
        // Required empty public constructor
    }

    public static ArtistsSongs newInstance() {
        ArtistsSongs fragment = new ArtistsSongs();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artists_songs, container, false);
        Log.d("myLogs", "ArtistsSongs");

        listView = (ListView) view.findViewById(R.id.artists__songs_lv);


        trackAdapter = new TrackAdapter(getContext(), showingStudios);
        listView.setAdapter(trackAdapter);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        try {
            SharedPreferences prfs = getActivity().getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
            String id = prfs.getString("id_artist", "");
            Log.d("myLogs", "id " + id);
            JSONArray data = new JSONArray(request.getHttpGet(api.completeUrl("tracks?salt=smdf9045jdsg54gjs54os59gjsog5453&&artist_id=" + id)));

            Gson gson = new Gson();
            tracks = gson.fromJson(data.toString(), new TypeToken<List<Track>>() {
            }.getType());

            showingStudios.clear();

            if (tracks != null) {

                for (int i = 0; i < tracks.size(); i++) {

                    Track track_url = tracks.get(i);
                    showingStudios.add(track_url);
                }
                trackAdapter.notifyDataSetChanged();


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {

//интенет
            }
        });


        return view;
    }

}


